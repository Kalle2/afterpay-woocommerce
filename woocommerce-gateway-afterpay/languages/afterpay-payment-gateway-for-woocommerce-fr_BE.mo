��                �      �     �     �     �                .     2     6     :     A     H     c     p     t     x     |     �     �     �     �     �     �      �     �     �  ^   �     :     ?  �  M     �     �       *   
     5     G     P     U     \     d      l  "   �     �     �     �     �     �     �     �     �     �     �  '   �            k   +     �     �    of AfterPay. Apr Aug Birthday is a required field Date of birth Day Dec Feb Female Gender Gender is a required field I accept the Jan Jul Jun Male Mar May Month Nov Oct Phone number Phone number is a required field Sep TEST MODE ENABLED There is a problem with submitting this order to AfterPay, please check the following issues:  Year payment terms Project-Id-Version: WooCommerce AfterPay Gateway
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-05-16 14:24+0000
PO-Revision-Date: 2020-11-27 09:13+0000
Last-Translator: 
Language-Team: French (Belgium)
Language: fr_BE
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.4; wp-5.5.3 de AfterPay. Avr. Août Date de naissance est un champ obligatoire Date de naissance Journée Dec. Févr. Femelle Le sexe Le sexe est un champ obligatoire J'ai lu et suis d'accord avec les  Janv. Juil. Juin Mâle Mars Mai Mois Nov. Oct. Téléphone Le téléphone est un champ obligatoire Sept. MODE TEST ACTIVÉ Il y a un problème avec l'envoi de cette commande à AfterPay, veuillez vérifier les problèmes suivants: Année conditions générales 