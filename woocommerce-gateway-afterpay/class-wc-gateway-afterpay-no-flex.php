<?php
/**
 * AfterPay Flex payment method for Norway
 *
 * @category   Class
 * @package    WC_Payment_Gateway
 * @author     arvato Finance B.V.
 * @copyright  since 2011 arvato Finance B.V.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * AfterPay Flex payment method for Norway
 *
 * @class         WC_Gateway_Afterpay_No_Flex
 * @extends       WC_Gateway_Afterpay_Base_Rest
 * @package       Arvato_AfterPay
 * @author        AfterPay
 */
class WC_Gateway_Afterpay_No_Flex extends WC_Gateway_Afterpay_Base_Rest {

	/**
	 * Constructor for the gateway.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		global $woocommerce;

		parent::__construct();

		$this->id           = 'afterpay_no_flex';
		$this->method_title = __( 'AfterPay Norway Flex', 'afterpay-payment-gateway-for-woocommerce' );
		$this->has_fields   = true;

		// Setup field to load available flex information in.
		$this->available_flex_information = array();

		// Load the form fields.
		$this->init_form_fields();
		$this->show_bankaccount = false;
		$this->order_type       = 'B2C';

		// Load the settings.
		$this->init_settings();

		// Define user set variables for basic settings.
		$this->enabled           = ( isset( $this->settings['enabled'] ) ) ?
			$this->settings['enabled'] : '';
		$this->title             = ( isset( $this->settings['title'] ) ) ?
			$this->settings['title'] : '';
		$this->extra_information = ( isset( $this->settings['extra_information'] ) ) ?
			$this->settings['extra_information'] : '';
		$this->merchantid        = ( isset( $this->settings['merchantid'] ) ) ?
			$this->settings['merchantid'] : '';
		$this->lower_threshold   = ( isset( $this->settings['lower_threshold'] ) ) ?
			$this->settings['lower_threshold'] : '';
		$this->upper_threshold   = ( isset( $this->settings['upper_threshold'] ) ) ?
			$this->settings['upper_threshold'] : '';
		$this->testmode          = ( isset( $this->settings['testmode'] ) ) ?
			$this->settings['testmode'] : '';
		$this->debug_mail        = ( isset( $this->settings['debug_mail'] ) ) ?
			$this->settings['debug_mail'] : '';
		$this->ip_restriction    = ( isset( $this->settings['ip_restriction'] ) ) ?
			$this->settings['ip_restriction'] : '';
		$this->show_advanced     = ( isset( $this->settings['show_advanced'] ) ) ?
			$this->settings['show_advanced'] : 'no';

		// Advanced settings.
		$this->show_phone                            = ( isset( $this->settings['show_phone'] ) ) ?
			$this->settings['show_phone'] : '';
		$this->show_termsandconditions               = ( isset( $this->settings['show_termsandconditions'] ) ) ?
			$this->settings['show_termsandconditions'] : '';
		$this->exclude_shipping_methods              = ( isset( $this->settings['exclude_shipping_methods'] ) ) ?
			$this->settings['exclude_shipping_methods'] : '';
		$this->use_custom_housenumber_field          = ( isset( $this->settings['use_custom_housenumber_field'] ) ) ?
			$this->settings['use_custom_housenumber_field'] : '';
		$this->use_custom_housenumber_addition_field =
			( isset( $this->settings['use_custom_housenumber_addition_field'] ) ) ?
			$this->settings['use_custom_housenumber_addition_field'] : '';
		$this->ip_restriction                        = ( isset( $this->settings['ip_restriction'] ) ) ?
			$this->settings['ip_restriction'] : '';

		// Captures and refunds.
		$this->captures                     = ( isset( $this->settings['captures'] ) ) ?
			$this->settings['captures'] : '';
		$this->captures_way                 = ( isset( $this->settings['captures_way'] ) ) ?
			$this->settings['captures_way'] : '';
		$this->captures_way_based_on_status = ( isset( $this->settings['captures_way_based_on_status'] ) ) ?
			$this->settings['captures_way_based_on_status'] : '';
		$this->refunds                      = ( isset( $this->settings['refunds'] ) ) ?
			$this->settings['refunds'] : '';
		$this->refund_tax_percentage        = ( isset( $this->settings['refund_tax_percentage'] ) ) ?
			$this->settings['refund_tax_percentage'] : '';

		if ( isset( $this->settings['refunds'] ) && 'yes' === $this->settings['refunds'] ) {
			$this->supports = array( 'refunds' );
		}

		$afterpay_country           = 'NO';
		$afterpay_language          = 'NO';
		$afterpay_currency          = 'NOK';
		$afterpay_invoice_terms     = 'https://documents.myafterpay.com/consumer-terms-conditions/no_no/' . $this->merchantid;
		$afterpay_installment_terms = '';
		$afterpay_privacy_statement = 'https://documents.myafterpay.com/privacy-statement/no_no/' . $this->merchantid;
		$afterpay_invoice_icon      = 'https://cdn.myafterpay.com/logo/AfterPay_logo.svg';

		// Apply filters to Country and language.
		$this->afterpay_country           = apply_filters( 'afterpay_country', $afterpay_country );
		$this->afterpay_language          = apply_filters( 'afterpay_language', $afterpay_language );
		$this->afterpay_currency          = apply_filters( 'afterpay_currency', $afterpay_currency );
		$this->afterpay_invoice_terms     = apply_filters( 'afterpay_invoice_terms', $afterpay_invoice_terms );
		$this->afterpay_installment_terms = apply_filters( 'afterpay_installment_terms', $afterpay_installment_terms );
		$this->afterpay_privacy_statement = apply_filters( 'afterpay_privacy_statement', $afterpay_privacy_statement );
		$this->icon                       = apply_filters( 'afterpay_invoice_icon', $afterpay_invoice_icon );

		// Actions.
		/* 2.0.0 */
		add_action(
			'woocommerce_update_options_payment_gateways_' . $this->id,
			array( $this, 'process_admin_options' )
		);

		add_action( 'woocommerce_receipt_afterpay', array( &$this, 'receipt_page' ) );

		// Add event handler for an order Status change.
		add_action( 'woocommerce_order_status_changed', array( $this, 'order_status_change_callback' ), 1000, 4 );
	}

	/**
	 * Initialise Gateway Settings Form Fields
	 *
	 * @access public
	 * @return void
	 */
	public function init_form_fields() {

		$this->form_fields = apply_filters(
			'afterpay_no_installments_form_fields', array(
				'enabled'                               => array(
					'title'   => __( 'Enable/Disable', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable AfterPay Norway Flex', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => 'no',
				),
				'title'                                 => array(
					'title'       => __( 'Title', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __( 'This controls the title which the user sees during checkout.', 'afterpay-payment-gateway-for-woocommerce' ),
					'default'     => 'AfterPay Flex – Betal i egen takt',
				),
				'extra_information'                     => array(
					'title'       => __( 'Extra information', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'textarea',
					'description' => __( 'Extra information to show to the customer in the checkout', 'afterpay-payment-gateway-for-woocommerce' ),
					'default'     => 'Samle alle kjøp på én månedsfaktura. Velg selv hvor mye du betaler hver måned.',
				),
				'api_key'                               => array(
					'title'       => __( 'API Key', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Please enter your AfterPay API Key; this is needed in order to take payment!',
						'afterpay'
					),
					'default'     => '',
				),
				'lower_threshold'                       => array(
					'title'       => __( 'Lower threshold', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Disable AfterPay Invoice if Cart Total is lower than the specified value. Leave blank to disable this feature.',
						'afterpay'
					),
					'default'     => '5',
				),
				'upper_threshold'                       => array(
					'title'       => __( 'Upper threshold', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Disable AfterPay Invoice if Cart Total is higher than the specified value. Leave blank to disable this feature.',
						'afterpay'
					),
					'default'     => '',
				),
				'testmode'                              => array(
					'title'       => __( 'Test Mode', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'select',
					'description' => __(
						'Enable or disable test mode. The sandbox option is for testing with API keys from developer.afterpay.io.',
						'afterpay'
					),
					'options'     => array(
						'yes'     => 'Yes',
						'sandbox' => 'Yes - Sandbox',
						'no'      => 'No',
					),
				),
				'debug_mail'                            => array(
					'title'       => __( 'Debug mail', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Use debug mail to send the complete AfterPay request to your mail, for debug functionality only. Leave empty to disable.',
						'afterpay'
					),
					'default'     => '',
				),
				'ip_restriction'                        => array(
					'title'       => __( 'IP restriction', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Fill in IP address to only show the payment method for that specific IP address. Leave empty to disable',
						'afterpay'
					),
					'default'     => '',
				),
				'show_advanced'                         => array(
					'title'       => __( 'Show advanced settings', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'select',
					'description' => __(
						'Show advanced settings'
					),
					'options'     => array(
						'yes' => 'Yes',
						'no'  => 'No',
					),
					'default'     => 'no',
				),
				'display_settings'                      => array(
					'title'       => __( 'Display settings', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'title',
					'description' => '',
					'class'       => 'afterpay_advanced_setting',
				),
				'show_phone'                            => array(
					'title'       => __( 'Show phone number', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'checkbox',
					'description' => __(
						'Show phone number field instead of Woocommerce default field. If this field is missing in default checkout',
						'afterpay'
					),
					'default'     => 'no',
					'class'       => 'afterpay_advanced_setting',
				),
				'show_termsandconditions'               => array(
					'title'       => __( 'Show terms and conditions', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'checkbox',
					'description' => __( 'Show terms and conditions of AfterPay', 'afterpay-payment-gateway-for-woocommerce' ),
					'default'     => 'yes',
					'class'       => 'afterpay_advanced_setting',
				),
				'exclude_shipping_methods'              => array(
					'title'       => __( 'Exclude shipping methods', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'multiselect',
					'description' => __( 'Do not show AfterPay if selected shipping methods are used', 'afterpay-payment-gateway-for-woocommerce' ),
					'options'     => $this->get_all_shipping_methods(),
					'default'     => 'yes',
					'class'       => 'afterpay_advanced_setting',
				),
				'use_custom_housenumber_field'          => array(
					'title'       => __( 'Use custom housenumber field', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Fill in the custom field name used for the housenumber, without billing_ or shipping_',
						'afterpay'
					),
					'default'     => '',
					'class'       => 'afterpay_advanced_setting',
				),
				'use_custom_housenumber_addition_field' => array(
					'title'       => __( 'Use custom housenumber addition field', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Fill in the custom field name used for the housenumber addition, without billing_ or shipping_',
						'afterpay'
					),
					'default'     => '',
					'class'       => 'afterpay_advanced_setting',
				),
				'merchantid'                            => array(
					'title'       => __( 'Merchant ID', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'default'     => '',
					'class'       => 'afterpay_advanced_setting',
					'description' => __('The merchant ID can be used for merchant specific terms and conditions.'),
				),
				'captures_and_refunds_section'          => array(
					'title'       => __( 'Captures and refunds', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'title',
					'description' => '<strong style="color:red">' . __( 'This section contains developer settings that can only be set in contact with a consultant of AfterPay. Please contact AfterPay for more information.' ) . '</strong>',
					'class'       => 'afterpay_advanced_setting',
				),
				'captures'                              => array(
					'title'   => __( 'Enable captures', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable capturing', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => 'yes',
					'class'   => 'afterpay_advanced_setting',
				),
				'captures_way'                          => array(
					'title'   => __( 'Way of captures', 'afterpay-payment-gateway-for-woocommerce' ),
					'label'   => __( 'Way of capturing', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'select',
					'default' => 'auto_after_authorization',
					'options' => array(
						'auto_after_authorization' => __( 'Automatically after authorization', 'afterpay-payment-gateway-for-woocommerce' ),
						'based_on_status'          => __( 'Based on Woocommerce Status', 'afterpay-payment-gateway-for-woocommerce' ),
					),
					'class'   => 'afterpay_advanced_setting',
				),
				'captures_way_based_on_status'          => array(
					'title'   => __( 'Status to capture based on', 'afterpay-payment-gateway-for-woocommerce' ),
					'label'   => __( 'Status to capture based on', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'select',
					'options' => $this->get_all_possible_order_statuses(),
					'class'   => 'afterpay_advanced_setting',
				),
				'refunds'                               => array(
					'title'   => __( 'Enable refunds', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable refunding', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => 'yes',
					'class'   => 'afterpay_advanced_setting',
				),
				'refund_tax_percentage'                 => array(
					'title'   => __( 'Refund tax percentage', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'text',
					'label'   => __( 'Default percentage calculated on refunds to AfterPay', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => '0',
					'class'   => 'afterpay_advanced_setting',
				),
			)
		);
	}

	/**
	 * Check if this gateway is enabled and available in the user's country
	 *
	 * @access public
	 * @return boolean
	 */
	public function is_available() {
		global $woocommerce;

		if ( 'yes' === $this->enabled && $woocommerce->cart ) {

			// Cart totals check - Lower threshold.
			if ( ! is_admin() && '' !== $this->lower_threshold ) {
				if ( $woocommerce->cart->subtotal < $this->lower_threshold ) {
					return false;
				}
			}
			// Cart totals check - Upper threshold.
			if ( ! is_admin() && '' !== $this->upper_threshold ) {
				if ( $woocommerce->cart->subtotal > $this->upper_threshold ) {
					return false;
				}
			}
			// Only activate the payment gateway if the customers country is the same as the filtered shop country.
			if ( ! is_admin() ) {
				if ( $woocommerce->customer->get_billing_country() !== $this->afterpay_country ) {
					return false;
				}
			}
			// Check if variable with ip's contains the ip of the client.
			if ( ! is_admin() && '' !== $this->ip_restriction ) {
				if ( strpos( $this->ip_restriction, $this->get_afterpay_client_ip() ) === false ) {
					return false;
				}
			}

			// Check if subtotal is more than 0, if setup request for available installment plans.
			if ( $woocommerce->cart->total > 0 ) {
				require_once __DIR__ . '/vendor/autoload.php';

				// Create AfterPay object.
				$afterpay = new \Afterpay\Afterpay();
				$afterpay->setRest();
				$afterpay->set_ordermanagement( 'available_payment_methods' );

				// Get connection mode.
				$afterpay_mode = $this->get_connection_mode();

				// Set up the additional information.
				$requestData = [
					'order' => [
						'totalGrossAmount' => round($woocommerce->cart->total, 2),
						'totalNetAmount' => round($woocommerce->cart->total - $woocommerce->cart->get_total_tax(), 2)
					]
				];
				$authorisation['apiKey'] = $this->settings['api_key'];
				$afterpay->set_order( $requestData, 'OM' );
				$afterpay->do_request( $authorisation, $afterpay_mode );

				// Check if the response from the webservice was correct, else the payment method is not available.
				if ( ! property_exists( $afterpay->order_result->return, 'paymentMethods' ) ) {
					return false;
				} else {
					$accountInformation = array();
					foreach ($afterpay->order_result->return->paymentMethods as $paymentMethod) {
						if ($paymentMethod->type === 'Account' && property_exists($paymentMethod, 'account')) {
							$accountInformation['monthlyFee'] = $paymentMethod->account->monthlyFee;
							$accountInformation['installmentAmount'] = $paymentMethod->account->installmentAmount;
							$accountInformation['interestRate'] = $paymentMethod->account->interestRate;
							$accountInformation['readMore'] = $paymentMethod->account->readMore;
						}
					}
					$this->available_flex_information = $accountInformation;
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Payment form on checkout page
	 *
	 * @acces public
	 * @return void
	 */
	public function payment_fields() {
		global $woocommerce;

		if ( 'yes' === $this->testmode ) : ?>
		<div style="background-color: red; color: white; margin: 10px; padding: 10px; text-align: center; font-weight: bold; text-shadow: none; border-radius: 10px"><?php esc_html_e( 'TEST MODE ENABLED', 'afterpay-payment-gateway-for-woocommerce' ); ?></div>
		<?php endif; ?>

		<?php if ( 'sandbox' === $this->testmode ) : ?>
		<div style="background-color: red; color: white; margin: 10px; padding: 10px; text-align: center; font-weight: bold; text-shadow: none; border-radius: 10px"><?php esc_html_e( 'TEST SANDBOX MODE ENABLED', 'afterpay-payment-gateway-for-woocommerce' ); ?></div>
		<?php endif; ?>

		<style>
			.installment_plan {
				display: none;
			}
			.installment_active {
				display: block;
			}
			.installment_plan ul li {
				list-style-type: disc;
			}
		</style>

		<fieldset>
			<?php if ( '' !== $this->extra_information ) : ?>
			<p class="form-row">
				<p> <?php echo esc_html( $this->extra_information ); ?></p>
			</p>
		<?php endif; ?>

			<p class="form-row">
				<p>Månedsebløp: kr <?php echo esc_html( round( $this->available_flex_information['installmentAmount'], 2 ) ); ?> eller ca 10 % av kredittbeløpet</p>
				<ul class="afterpay_installment_bullets">
					<li style="list-style-type: circle !important;">Ingen etableringsgebyr</li>
					<li style="list-style-type: circle !important;">Månedsgebyr: kr <?php echo esc_html( number_format( $this->available_flex_information['monthlyFee'], 2 ) ); ?> / kk</li>
					<li style="list-style-type: circle !important;">Årlig rente : <?php echo esc_html( $this->available_flex_information['interestRate'] ); ?> %</li>
				</ul>
				<p>Søknad om delbetaling må fullføres etter at kjøpet er gjort.</p>
                <br/>
				<p>Eksempel: For et kjøp på 1000 kr som betales over 12 måneder, er totalkostnad 1085 kr og effektiv rente 16,49%. Les <a href="<?php echo $this->available_flex_information['readMore']; ?>" target="blank">Standard European Consumer Credit Information</a></p>
				<br/>
			</p>
			<div class="clear"></div>
			<p class="form-row">
				<label for="<?php echo esc_attr( $this->id ); ?>_ssn">Tast inn ditt personnummer (11 siffer) for å fullføre kjøpet <span class="required">*</span></label>
				<input type="input" class="input-text" name="<?php echo esc_attr( $this->id ); ?>_ssn" placeholder="DDMMÅÅXXXXX"/>
				For mer informasjon om behandling av persondata, <a href="<?php echo esc_url( $this->afterpay_privacy_statement ); ?>" target="blank">klikk her</a>.
			</p>
			<div class="clear"></div>
			<?php if ( 'yes' === $this->show_phone ) : ?>
			<div class="clear"></div>
			<p class="form-row form-row-first validate-required validate-phone">
				<label for="<?php echo esc_attr( $this->id ); ?>_phone"><?php echo esc_html_e( 'Phone number', 'afterpay-payment-gateway-for-woocommerce' ); ?><span class="required">*</span></label>
				<input type="input" class="input-text" name="<?php echo esc_attr( $this->id ); ?>_phone" />
			</p>
			<?php endif; ?>
			<?php if ( 'yes' === $this->show_termsandconditions ) : ?>
			<div class="clear"></div>
			<p class="form-row validate-required">
				<input type="checkbox" class="input-checkbox" name="<?php echo esc_attr( $this->id ); ?>_terms" /><span class="required">*</span>
				Jeg godkjenner <a href="' . esc_url( $this->afterpay_invoice_terms ) . '" target="blank">betingelsene</a> til AfterPay.
			</p>
			<?php endif; ?>
		</fieldset>
		<?php
	}

	/**
	 * Validate form fields.
	 *
	 * @access public
	 * @return boolean
	 */
	public function validate_fields() {
		global $woocommerce;

		if ( 'yes' === $this->show_phone && ! isset( $_POST[ $this->id . '_phone' ] ) ) {
			wc_add_notice( __( 'Phone number is a required field', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( 'yes' === $this->show_termsandconditions && ! isset( $_POST[ $this->id . '_terms' ] ) ) {
			wc_add_notice( __( 'Please accept the AfterPay terms.', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( ! isset( $_POST[ $this->id . '_ssn' ] ) ) {
			wc_add_notice( __( 'Social security number is a required field', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( ! isset( $_POST[ $this->id . '_installmentplan' ] ) ) {
			wc_add_notice( __( 'Please choose an installment plan.', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		return true;
	}
}
