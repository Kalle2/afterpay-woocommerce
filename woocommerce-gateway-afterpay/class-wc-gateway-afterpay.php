<?php
/**
 * AfterPay payment gateway for Woocommerce
 *
 * @package    WC_Payment_Gateway
 * @author     AfterPay
 * @copyright  since 2011 arvato Finance B.V.
 *
 * @wordpress-plugin
 * Plugin Name: AfterPay payment gateway for Woocommerce
 * Plugin URI: https://www.afterpay.nl/en
 * Description: Extends WooCommerce. Provides a <a href="http://www.afterpay.nl/en" target="_blank">AfterPay</a> gateway for WooCommerce.
 * Version: 5.3.0
 * Author: AfterPay
 * Author URI: https://developer.afterpay.io
 * Text Domain: afterpay-payment-gateway-for-woocommerce
 * Domain Path: /languages
 * WC tested up to: 5.3.0
 * WC requires at least: 4.5.0
 */

/**
 * Copyright (c) copyright  since 2011 arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly..
}

/**
 * Check if WooCommerce is active
 */
$woo_active = false;
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ), true ) ) {
	$woo_active = true;
}
if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
	require_once ABSPATH . '/wp-admin/includes/plugin.php';
}
if ( is_plugin_active_for_network( 'woocommerce/woocommerce.php' ) && true !== $woo_active ) {
	$woo_active = true;
}
if ( false === $woo_active ) {
	echo 'The WooCommerce plugin should be installed before using the AfterPay Woocommerce Plugin';
	exit;
}

/**
 * Load AfterPay extension on the WC Coupons to add specific AfterPay Coupon
 * field needed for Customer Individual Score
 */
require_once 'class-afterpay-coupon.php';

/**
 * Initiate AfterPay Gateway, load necessary files and classes.
 *
 * @access public
 * @return void
 **/
function init_afterpay_gateway() {

	if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
		return;
	}

	/**
	 * Localisation
	 */
	load_plugin_textdomain( 'afterpay-payment-gateway-for-woocommerce', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	// Define AfterPay root Dir.
	define( 'AFTERPAY_DIR', dirname( __FILE__ ) . '/' );

	// Define AfterPay lib Dir.
	define( 'AFTERPAY_LIB', dirname( __FILE__ ) . '/vendor/payintegrator/afterpay/lib/' );

	/**
	 * AfterPay Payment Gateway
	 *
	 * @class          WC_Gateway_Afterpay
	 * @extends        WC_Payment_Gateway
	 * @package        WooCommerce/Classes/Payment
	 * @author         Willem Fokkens
	 */
	class WC_Gateway_Afterpay extends WC_Payment_Gateway {

		/**
		 * Constructor for the gateway.
		 *
		 * @access public
		 * @return void
		 */
		public function __construct() {
			global $woocommerce;
		}
	}

	// Load the AfterPay Base class and then the country specific payment methods.
	require_once 'class-wc-gateway-afterpay-base.php';

	// Netherlands.
	require_once 'class-wc-gateway-afterpay-nl-openinvoice.php';
	require_once 'class-wc-gateway-afterpay-nl-directdebit.php';
	require_once 'class-wc-gateway-afterpay-nl-business.php';
	require_once 'class-wc-gateway-afterpay-nl-openinvoice-extra.php';
	require_once 'class-wc-gateway-afterpay-nl-business-extra.php';

	// Belgium.
	require_once 'class-wc-gateway-afterpay-be-openinvoice.php';

	// Load the AfterPay Base class for the countries using REST interface.
	require_once 'class-wc-gateway-afterpay-base-rest.php';

	// Netherlands.
	require_once 'class-wc-gateway-afterpay-nl-openinvoice-rest.php';

	// Belgium.
	require_once 'class-wc-gateway-afterpay-be-openinvoice-rest.php';

	// Germany.
	require_once 'class-wc-gateway-afterpay-de-openinvoice.php';
	require_once 'class-wc-gateway-afterpay-de-installments.php';
	require_once 'class-wc-gateway-afterpay-de-directdebit.php';
	require_once 'class-wc-gateway-afterpay-de-openinvoice-extra.php';

	// Austria.
	require_once 'class-wc-gateway-afterpay-at-openinvoice.php';
	require_once 'class-wc-gateway-afterpay-at-installments.php';
	require_once 'class-wc-gateway-afterpay-at-directdebit.php';

	// Switzerland.
	require_once 'class-wc-gateway-afterpay-ch-openinvoice.php';

	// Denmark.
	require_once 'class-wc-gateway-afterpay-dk-openinvoice.php';
	require_once 'class-wc-gateway-afterpay-dk-installments.php';
	require_once 'class-wc-gateway-afterpay-dk-flex.php';

	// Sweden.
	require_once 'class-wc-gateway-afterpay-se-openinvoice.php';
	require_once 'class-wc-gateway-afterpay-se-installments.php';
	require_once 'class-wc-gateway-afterpay-se-flex.php';

	// Finland.
	require_once 'class-wc-gateway-afterpay-fi-openinvoice.php';
	require_once 'class-wc-gateway-afterpay-fi-installments.php';
	require_once 'class-wc-gateway-afterpay-fi-flex.php';

	// Norway.
	require_once 'class-wc-gateway-afterpay-no-openinvoice.php';
	require_once 'class-wc-gateway-afterpay-no-installments.php';
	require_once 'class-wc-gateway-afterpay-no-flex.php';

}

// If plugins are loaded then initiate AfterPay Gateway.
add_action( 'plugins_loaded', 'init_afterpay_gateway', 0 );

/**
 * Add the gateway to WooCommerce
 *
 * @access public
 * @param array $methods Woocommerce Payment Gateways.
 * @return array
 **/
function add_afterpay_gateway( $methods ) {

	$country_obj = new WC_Countries();
	$countries = $country_obj->get_allowed_countries();
	$methods = array();	

	if(array_key_exists('NL',$countries)) {
		// The Netherlands.
		$methods[] = 'WC_Gateway_Afterpay_Nl_Openinvoice';
		$methods[] = 'WC_Gateway_Afterpay_Nl_Directdebit';
		$methods[] = 'WC_Gateway_Afterpay_Nl_Business';
		$methods[] = 'WC_Gateway_Afterpay_Nl_Openinvoice_Extra';
		$methods[] = 'WC_Gateway_Afterpay_Nl_Business_Extra';
		$methods[] = 'WC_Gateway_Afterpay_Nl_Openinvoice_Rest';
	}
	
	if(array_key_exists('BE',$countries)) {
		// Belgium.
		$methods[] = 'WC_Gateway_Afterpay_Be_Openinvoice';
		$methods[] = 'WC_Gateway_Afterpay_Be_Openinvoice_Rest';
	}

	if(array_key_exists('DE',$countries)) {
		// Germany.
		$methods[] = 'WC_Gateway_Afterpay_De_Openinvoice';
		$methods[] = 'WC_Gateway_Afterpay_De_Directdebit';
		$methods[] = 'WC_Gateway_Afterpay_De_Installments';
		$methods[] = 'WC_Gateway_Afterpay_De_Openinvoice_Extra';
	}

	if(array_key_exists('AT',$countries)) {
		// Austria.
		$methods[] = 'WC_Gateway_Afterpay_At_Openinvoice';
		$methods[] = 'WC_Gateway_Afterpay_At_Installments';
		$methods[] = 'WC_Gateway_Afterpay_At_Directdebit';
	}

	if(array_key_exists('CH',$countries)) {
		// Switzerland.
		$methods[] = 'WC_Gateway_Afterpay_Ch_Openinvoice';
	}

	if(array_key_exists('DK',$countries)) {
		// Denmark.
		$methods[] = 'WC_Gateway_Afterpay_Dk_Openinvoice';
		$methods[] = 'WC_Gateway_Afterpay_Dk_Installments';
		$methods[] = 'WC_Gateway_Afterpay_Dk_Flex';
	}

	if(array_key_exists('SE',$countries)) {
		// Sweden.
		$methods[] = 'WC_Gateway_Afterpay_Se_Openinvoice';
		$methods[] = 'WC_Gateway_Afterpay_Se_Installments';
		$methods[] = 'WC_Gateway_Afterpay_Se_Flex';
	}

	if(array_key_exists('FI',$countries)) {
		// Finland.
		$methods[] = 'WC_Gateway_Afterpay_Fi_Openinvoice';
		$methods[] = 'WC_Gateway_Afterpay_Fi_Installments';
		$methods[] = 'WC_Gateway_Afterpay_Fi_Flex';
	}

	if(array_key_exists('NO',$countries)) {
		// Norway.
		$methods[] = 'WC_Gateway_Afterpay_No_Openinvoice';
		$methods[] = 'WC_Gateway_Afterpay_No_Installments';
		$methods[] = 'WC_Gateway_Afterpay_No_Flex';
	}

	return $methods;
}

/**
 * AfterPay return page and order check
 *
 * @access public
 **/
function afterpay_return() {
	if ( isset( $_SERVER['REQUEST_URI'] ) ) {
	
		$request_uri = esc_url_raw( wp_unslash( $_SERVER['REQUEST_URI'] ) );
		if ( strpos( $request_uri, '/afterpay/return' ) !== false ) {
			$order_id = null;
			if ( isset( $_GET['order_id'] ) ) {
				$order_id = sanitize_text_field( wp_unslash( $_GET['order_id'] ) );
			} else {
				exit();
			}

			$order = wc_get_order( $order_id );
			
			// Check if the order is valid.
			if ( ! is_object( $order ) ) {
				exit();
			}
			// Check if the order has the status 'pending'.
			if ( $order->get_status() !== 'pending' ) {
				exit();
			}

			// Check if payment method is AfterPay.
			if ( strpos( $order->get_payment_method(), 'afterpay' ) === false ) {
				exit();
			}

			// Get an instance of the WC_Payment_Gateways object.
			$payment_gateways = WC_Payment_Gateways::instance();

			// Get the desired WC_Payment_Gateway object.
			$payment_gateway = $payment_gateways->payment_gateways()[ $order->get_payment_method() ];
			$status          = $payment_gateway->afterpay_check_status( $order );

			if ( 'Accepted' === $status ) {
				$order->add_order_note( __( 'Order is accepted after AfterPay SCA', 'afterpay-payment-gateway-for-woocommerce' ) );

				if (
					isset( $payment_gateway->settings['captures'] )
					&& 'yes' === $payment_gateway->settings['captures']
					&& isset( $payment_gateway->settings['captures_way'] )
					&& 'auto_after_authorization' === $payment_gateway->settings['captures_way']
				) {
					// Capture payment.
					$payment_gateway->capture_afterpay_payment( null, $order );
				}

				if (
					isset( $payment_gateway->settings['captures'] )
					&& 'yes' === $payment_gateway->settings['captures']
					&& isset( $payment_gateway->settings['captures_way'] )
					&& 'auto_after_authorization' !== $payment_gateway->settings['captures_way']
				) {
					// Add note that the order is not captured yet.
					$order->add_order_note( __( 'AfterPay capture needed, since the Capture mode was set to(Based on Woocommerce Status) when the order was placed.', 'afterpay-payment-gateway-for-woocommerce' ) );
				}

				$order->payment_complete();
				header( 'Location: ' . $order->get_checkout_order_received_url() );
				exit();
			} else {
				$order->add_order_note( __( 'Strong authentication failed or was aborted. Please try again or select another payment method.', 'afterpay-payment-gateway-for-woocommerce' ) );
				wc_add_notice( 'Strong authentication failed or was aborted. Please try again or select another payment method.', 'error' );
				WC()->session->set( 'order_awaiting_payment', false );
				$order->update_status( 'cancelled', '' );
				header( 'Location: ' . wc_get_checkout_url() );
				exit();
			}
		}
	}
}

/**
 * AfterPay cron and order check
 *
 * @access public
 **/
function afterpay_croncheck() {

	// Get all enabled AfterPay gateways.
	$gateways         = WC_Payment_Gateways::instance();
	$enabled_gateways = [];
	$ignored_gateways = [
		'afterpay_openinvoice',
		'afterpay_openinvoice_extra',
		'afterpay_directdebit',
		'afterpay_business',
		'afterpay_business_extra',
		'afterpay_belgium',
	];

	if ( $gateways->payment_gateways ) {
		foreach ( $gateways->payment_gateways as $gateway ) {
			if ( 'yes' === $gateway->enabled && strpos( $gateway->id, 'afterpay' ) !== false ) {
				$enabled_gateways[] = $gateway->id;
			}
		}
	}

	$enabled_gateways = array_diff( $enabled_gateways, $ignored_gateways );

	foreach ( $enabled_gateways as $enabled_gateway ) {
		$query = new WC_Order_Query();

		// Check only orders that are 'pending payment'.
		$query->set( 'status', 'pending' );

		// Check only orders from AfterPay.
		$query->set( 'payment_method', $enabled_gateway );

		// Check only orders are older than 30 minutes.
		$query->set( 'date_created', '<' . ( time() - 1800 ) );

		$orders = $query->get_orders();

		if ( count( $orders ) > 0 ) {
			// Get payment gateway object.
			$payment_gateway = $gateways->payment_gateways()[ $enabled_gateway ];
			foreach ( $orders as $order ) {
				$status = $payment_gateway->afterpay_check_status( $order );
				if ( 'Accepted' === $status ) {
					if (
						isset( $payment_gateway->settings['captures'] )
						&& 'yes' === $payment_gateway->settings['captures']
						&& isset( $payment_gateway->settings['captures_way'] )
						&& 'auto_after_authorization' === $payment_gateway->settings['captures_way']
					) {
						// Capture payment.
						$payment_gateway->capture_afterpay_payment( null, $order );
					}
					$order->add_order_note( __( 'Order is accepted after AfterPay SCA', 'afterpay-payment-gateway-for-woocommerce' ) );
					$order->add_order_note( __( 'AfterPay payment completed.', 'afterpay-payment-gateway-for-woocommerce' ) );

					if (
						isset( $payment_gateway->settings['captures'] )
						&& 'yes' === $payment_gateway->settings['captures']
						&& isset( $payment_gateway->settings['captures_way'] )
						&& 'auto_after_authorization' !== $payment_gateway->settings['captures_way']
					) {
						// Add note that the order is not captured yet.
						$order->add_order_note( __( 'AfterPay capture needed, since the Capture mode was set to(Based on Woocommerce Status) when the order was placed.', 'afterpay-payment-gateway-for-woocommerce' ) );
					}

					$order->payment_complete();
				} else {
					$order->add_order_note( __( 'Strong authentication failed or was aborted.', 'afterpay-payment-gateway-for-woocommerce' ) );
					$order->update_status( 'cancelled', '' );
				}
			}
		}
	}
}


// Add AfterPay gateway to Woocommerce filters.
add_filter( 'woocommerce_payment_gateways', 'add_afterpay_gateway' );

/**
 * Initiate AfterPay Gateway css styles.
 *
 * @access public
 * @return void
 **/
function init_afterpay_styles() {

	wp_register_style( 'afterpay', plugins_url( basename( dirname( __FILE__ ) ) . '/css/styles.css' ) );
	wp_enqueue_style( 'afterpay' );

}

add_action( 'wp_enqueue_scripts', 'init_afterpay_styles' );
add_action( 'parse_request', 'afterpay_return' );

// Check status of pending orders based on WP Cron scheduler.
add_action( 'afterpay_check_pending', 'afterpay_croncheck' );

if ( ! wp_next_scheduled( 'afterpay_check_pending' ) ) {
	wp_schedule_event( time(), 'hourly', 'afterpay_check_pending' );
}
