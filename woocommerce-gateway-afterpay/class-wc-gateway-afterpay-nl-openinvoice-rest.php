<?php
/**
 * AfterPay Open invoice payment method for Netherlands using REST
 *
 * @category   Class
 * @package    WC_Payment_Gateway
 * @author     arvato Finance B.V.
 * @copyright  since 2011 arvato Finance B.V.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * AfterPay Open invoice payment method for Netherlands using REST
 *
 * @class       WC_Gateway_Afterpay_Nl_Openinvoice_Rest
 * @extends     WC_Gateway_Afterpay_Base_Rest
 * @package     Arvato_AfterPay
 * @author      AfterPay
 */
class WC_Gateway_Afterpay_Nl_Openinvoice_Rest extends WC_Gateway_Afterpay_Base_Rest {

	/**
	 * Constructor for the gateway.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		global $woocommerce;

		parent::__construct();

		$this->id           = 'afterpay_nl_openinvoice_rest';
		$this->method_title = __( 'AfterPay Netherlands Digital Invoice REST', 'afterpay-payment-gateway-for-woocommerce' );
		$this->has_fields   = true;

		// Load the form fields.
		$this->init_form_fields();
		$this->show_bankaccount = false;
		$this->order_type       = 'B2C';

		// Load the settings.
		$this->init_settings();

		// Define user set variables for basic settings.
		$this->enabled           = ( isset( $this->settings['enabled'] ) ) ?
			$this->settings['enabled'] : '';
		$this->title             = ( isset( $this->settings['title'] ) ) ?
			$this->settings['title'] : '';
		$this->extra_information = ( isset( $this->settings['extra_information'] ) ) ?
			$this->settings['extra_information'] : '';
		$this->merchantid        = ( isset( $this->settings['merchantid'] ) ) ?
			$this->settings['merchantid'] : '';
		$this->lower_threshold   = ( isset( $this->settings['lower_threshold'] ) ) ?
			$this->settings['lower_threshold'] : '';
		$this->upper_threshold   = ( isset( $this->settings['upper_threshold'] ) ) ?
			$this->settings['upper_threshold'] : '';
		$this->testmode          = ( isset( $this->settings['testmode'] ) ) ?
			$this->settings['testmode'] : '';
		$this->debug_mail        = ( isset( $this->settings['debug_mail'] ) ) ?
			$this->settings['debug_mail'] : '';
		$this->ip_restriction    = ( isset( $this->settings['ip_restriction'] ) ) ?
			$this->settings['ip_restriction'] : '';
		$this->show_advanced     = ( isset( $this->settings['show_advanced'] ) ) ?
			$this->settings['show_advanced'] : 'no';

		// Advanced settings.
		$this->show_phone                            = ( isset( $this->settings['show_phone'] ) ) ?
			$this->settings['show_phone'] : '';
		$this->show_dob                              = ( isset( $this->settings['show_dob'] ) ) ?
			$this->settings['show_dob'] : '';
		$this->show_gender                           = ( isset( $this->settings['show_gender'] ) ) ?
			$this->settings['show_gender'] : '';
		$this->show_termsandconditions               = 'yes';
		$this->exclude_shipping_methods              = ( isset( $this->settings['exclude_shipping_methods'] ) ) ?
			$this->settings['exclude_shipping_methods'] : '';
		$this->use_custom_housenumber_field          = ( isset( $this->settings['use_custom_housenumber_field'] ) ) ?
			$this->settings['use_custom_housenumber_field'] : '';
		$this->use_custom_housenumber_addition_field =
			( isset( $this->settings['use_custom_housenumber_addition_field'] ) ) ?
			$this->settings['use_custom_housenumber_addition_field'] : '';

		// Captures and refunds.
		$this->captures                     = ( isset( $this->settings['captures'] ) ) ?
			$this->settings['captures'] : '';
		$this->captures_way                 = ( isset( $this->settings['captures_way'] ) ) ?
			$this->settings['captures_way'] : '';
		$this->captures_way_based_on_status = ( isset( $this->settings['captures_way_based_on_status'] ) ) ?
			$this->settings['captures_way_based_on_status'] : '';
		$this->refunds                      = ( isset( $this->settings['refunds'] ) ) ?
			$this->settings['refunds'] : '';
		$this->refund_tax_percentage        = ( isset( $this->settings['refund_tax_percentage'] ) ) ?
			$this->settings['refund_tax_percentage'] : '';

		if ( isset( $this->settings['refunds'] ) && 'yes' === $this->settings['refunds'] ) {
			$this->supports = array( 'refunds' );
		}

		$afterpay_country       = 'NL';
		$afterpay_language      = 'NL';
		$afterpay_currency      = 'EUR';
		$afterpay_invoice_terms = 'https://www.afterpay.nl/nl/algemeen/betalen-met-afterpay/betalingsvoorwaarden';
		$afterpay_invoice_icon  = 'https://cdn.myafterpay.com/logo/AfterPay_logo.svg';

		// Apply filters to Country and language.
		$this->afterpay_country       = apply_filters( 'afterpay_country', $afterpay_country );
		$this->afterpay_language      = apply_filters( 'afterpay_language', $afterpay_language );
		$this->afterpay_currency      = apply_filters( 'afterpay_currency', $afterpay_currency );
		$this->afterpay_invoice_terms = apply_filters( 'afterpay_invoice_terms', $afterpay_invoice_terms );
		$this->icon                   = apply_filters( 'afterpay_invoice_icon', $afterpay_invoice_icon );

		// Actions.
		/* 2.0.0 */
		add_action(
			'woocommerce_update_options_payment_gateways_' . $this->id,
			array( $this, 'process_admin_options' )
		);

		add_action( 'woocommerce_receipt_afterpay', array( &$this, 'receipt_page' ) );

		// Add event handler for an order Status change.
		add_action( 'woocommerce_order_status_changed', array( $this, 'order_status_change_callback' ), 1000, 4 );
	}

	/**
	 * Initialise Gateway Settings Form Fields
	 *
	 * @access public
	 * @return void
	 */
	public function init_form_fields() {
		$this->form_fields = apply_filters(
			'afterpay_nl_openinvoice_rest_form_fields', array(
				'enabled'                               => array(
					'title'   => __( 'Enable/Disable', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable AfterPay Netherlands Digital Invoice REST', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => 'no',
				),
				'title'                                 => array(
					'title'       => __( 'Title', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __( 'This controls the title which the user sees during checkout.', 'afterpay-payment-gateway-for-woocommerce' ),
					'default'     => __( 'Veilig achteraf betalen', 'afterpay-payment-gateway-for-woocommerce' ),
				),
				'extra_information'                     => array(
					'title'       => __( 'Extra information', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'textarea',
					'description' => __( 'Extra information to show to the customer in the checkout', 'afterpay-payment-gateway-for-woocommerce' ),
					'default'     => '',
				),
				'api_key'                               => array(
					'title'       => __( 'API Key', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Please enter your AfterPay API Key; this is needed in order to take payment!',
						'afterpay'
					),
					'default'     => '',
				),
				'lower_threshold'                       => array(
					'title'       => __( 'Lower threshold', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Disable AfterPay Invoice if Cart Total is lower than the specified value. Leave blank to disable this feature.',
						'afterpay'
					),
					'default'     => '5',
				),
				'upper_threshold'                       => array(
					'title'       => __( 'Upper threshold', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Disable AfterPay Invoice if Cart Total is higher than the specified value. Leave blank to disable this feature.',
						'afterpay'
					),
					'default'     => '',
				),
				'testmode'                              => array(
					'title'       => __( 'Test Mode', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'select',
					'description' => __(
						'Enable or disable test mode. The sandbox option is for testing with API keys from developer.afterpay.io.',
						'afterpay'
					),
					'options'     => array(
						'yes'     => 'Yes',
						'sandbox' => 'Yes - Sandbox',
						'no'      => 'No',
					),
				),
				'debug_mail'                            => array(
					'title'       => __( 'Debug mail', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Use debug mail to send the complete AfterPay request to your mail, for debug functionality only. Leave empty to disable.',
						'afterpay'
					),
					'default'     => '',
				),
				'ip_restriction'                        => array(
					'title'       => __( 'IP restriction', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Fill in IP address to only show the payment method for that specific IP address. Leave empty to disable',
						'afterpay'
					),
					'default'     => '',
				),
				'show_advanced'                         => array(
					'title'       => __( 'Show advanced settings', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'select',
					'description' => __(
						'Show advanced settings'
					),
					'options'     => array(
						'yes' => 'Yes',
						'no'  => 'No',
					),
					'default'     => 'no',
				),
				'display_settings'                      => array(
					'title'       => __( 'Display settings', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'title',
					'description' => '',
					'class'       => 'afterpay_advanced_setting',
				),
				'show_phone'                            => array(
					'title'       => __( 'Show phone number', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'checkbox',
					'description' => __(
						'Show phone number field instead of Woocommerce default field. If this field is missing in default checkout',
						'afterpay'
					),
					'default'     => 'no',
					'class'       => 'afterpay_advanced_setting',
				),
				'show_dob'                              => array(
					'title'       => __( 'Show date of birth', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'checkbox',
					'description' => __(
						'Show date of birth field in AfterPay form in the checkout',
						'afterpay'
					),
					'default'     => 'yes',
					'class'       => 'afterpay_advanced_setting',
				),
				'show_gender'                           => array(
					'title'       => __( 'Show gender', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'checkbox',
					'description' => __(
						'Show gender field in AfterPay form in the checkout',
						'afterpay'
					),
					'default'     => 'no',
					'class'       => 'afterpay_advanced_setting',
				),
				'exclude_shipping_methods'              => array(
					'title'       => __( 'Exclude shipping methods', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'multiselect',
					'description' => __( 'Do not show AfterPay if selected shipping methods are used', 'afterpay-payment-gateway-for-woocommerce' ),
					'options'     => $this->get_all_shipping_methods(),
					'default'     => 'yes',
					'class'       => 'afterpay_advanced_setting',
				),
				'use_custom_housenumber_field'          => array(
					'title'       => __( 'Use custom housenumber field', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Fill in the custom field name used for the housenumber, without billing_ or shipping_',
						'afterpay'
					),
					'default'     => '',
					'class'       => 'afterpay_advanced_setting',
				),
				'use_custom_housenumber_addition_field' => array(
					'title'       => __( 'Use custom housenumber addition field', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Fill in the custom field name used for the housenumber addition, without billing_ or shipping_',
						'afterpay'
					),
					'default'     => '',
					'class'       => 'afterpay_advanced_setting',
				),
				'merchantid'                            => array(
					'title'       => __( 'Merchant ID', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'default'     => '',
					'class'       => 'afterpay_advanced_setting',
					'description' => __('The merchant ID can be used for merchant specific terms and conditions.'),
				),
				'captures_and_refunds_section'          => array(
					'title'       => __( 'Captures and refunds', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'title',
					'description' => '<strong style="color:red">' . __( 'This section contains developer settings that can only be set in contact with a consultant of AfterPay. Please contact AfterPay for more information.' ) . '</strong>',
					'class'       => 'afterpay_advanced_setting',
				),
				'captures'                              => array(
					'title'   => __( 'Enable captures', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable capturing', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => 'yes',
					'class'   => 'afterpay_advanced_setting',
				),
				'captures_way'                          => array(
					'title'   => __( 'Way of captures', 'afterpay-payment-gateway-for-woocommerce' ),
					'label'   => __( 'Way of capturing', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'select',
					'default' => 'auto_after_authorization',
					'options' => array(
						'auto_after_authorization' => __( 'Automatically after authorization', 'afterpay-payment-gateway-for-woocommerce' ),
						'based_on_status'          => __( 'Based on Woocommerce Status', 'afterpay-payment-gateway-for-woocommerce' ),
					),
					'class'   => 'afterpay_advanced_setting',
				),
				'captures_way_based_on_status'          => array(
					'title'   => __( 'Status to capture based on', 'afterpay-payment-gateway-for-woocommerce' ),
					'label'   => __( 'Status to capture based on', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'select',
					'options' => $this->get_all_possible_order_statuses(),
					'class'   => 'afterpay_advanced_setting',
				),
				'refunds'                               => array(
					'title'   => __( 'Enable refunds', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable refunding', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => 'yes',
					'class'   => 'afterpay_advanced_setting',
				),
				'refund_tax_percentage'                 => array(
					'title'   => __( 'Refund tax percentage', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'text',
					'label'   => __( 'Default percentage calculated on refunds to AfterPay', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => '0',
					'class'   => 'afterpay_advanced_setting',
				),
			)
		);
	}

	/**
	 * Payment form on checkout page
	 *
	 * @acces public
	 * @return void
	 */
	public function payment_fields() {
		global $woocommerce;
		?>
		<?php if ( 'yes' === $this->testmode ) : ?>
		<div style="background-color: red; color: white; margin: 10px; padding: 10px; text-align: center; font-weight: bold; text-shadow: none; border-radius: 10px"><?php esc_html_e( 'TEST MODE ENABLED', 'afterpay-payment-gateway-for-woocommerce' ); ?></div>
		<?php endif; ?>

		<?php if ( 'sandbox' === $this->testmode ) : ?>
		<div style="background-color: red; color: white; margin: 10px; padding: 10px; text-align: center; font-weight: bold; text-shadow: none; border-radius: 10px"><?php esc_html_e( 'TEST SANDBOX MODE ENABLED', 'afterpay-payment-gateway-for-woocommerce' ); ?></div>
		<?php endif; ?>

		<?php if ( '' !== $this->extra_information ) : ?>
		<p> <?php echo esc_html( $this->extra_information ); ?></p>
		<?php endif; ?>

		<fieldset>
			<?php if ( 'yes' === $this->show_gender ) : ?>
			<p class="form-row">
				<label for="<?php echo esc_attr( $this->id ); ?>_gender"><?php esc_html_e( 'Gender', 'afterpay-payment-gateway-for-woocommerce' ); ?> <span class="required">*</span></label>
				<span class="gender">
					<select class="gender_select" name="<?php echo esc_attr( $this->id ); ?>_gender">
						<option value="V" selected><?php esc_html_e( 'Female', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="M"><?php esc_html_e( 'Male', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
					</select>
				</span>
			</p>
			<div class="clear"></div>
			<?php endif; ?>
			<?php if ( 'yes' === $this->show_dob ) : ?>
			<div class="clear"></div>
			<p class="form-row">
				<label for="<?php echo esc_attr( $this->id ); ?>_pno"><?php esc_html_e( 'Date of birth', 'afterpay-payment-gateway-for-woocommerce' ); ?> <span class="required">*</span></label>
				<span class="dob">
					<select class="dob_select dob_day" name="<?php echo esc_attr( $this->id ); ?>_date_of_birth_day">
						<option value="">
						<?php esc_html_e( 'Day', 'afterpay-payment-gateway-for-woocommerce' ); ?>
						</option>
						<?php
							$day = 1;
						while ( $day <= 31 ) {
							$day_pad = str_pad( $day, 2, '0', STR_PAD_LEFT );
							echo '<option value="' . esc_attr( $day_pad ) . '">' . esc_html( $day_pad ) . '</option>';
							$day++;
						}
						?>
					</select>
					<select class="dob_select dob_month" name="<?php echo esc_attr( $this->id ); ?>_date_of_birth_month">
						<option value="">
						<?php esc_html_e( 'Month', 'afterpay-payment-gateway-for-woocommerce' ); ?>
						</option>
						<option value="01"><?php esc_html_e( 'Jan', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="02"><?php esc_html_e( 'Feb', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="03"><?php esc_html_e( 'Mar', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="04"><?php esc_html_e( 'Apr', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="05"><?php esc_html_e( 'May', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="06"><?php esc_html_e( 'Jun', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="07"><?php esc_html_e( 'Jul', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="08"><?php esc_html_e( 'Aug', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="09"><?php esc_html_e( 'Sep', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="10"><?php esc_html_e( 'Oct', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="11"><?php esc_html_e( 'Nov', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="12"><?php esc_html_e( 'Dec', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
					</select>
					<select class="dob_select dob_year" name="<?php echo esc_attr( $this->id ); ?>_date_of_birth_year">
						<option value="">
						<?php esc_html_e( 'Year', 'afterpay-payment-gateway-for-woocommerce' ); ?>
						</option>
						<?php
							// Select current date and deduct 18 years because of the date limit of using AfterPay.
							$year = date( 'Y' ) - 18;
							// Select the oldest year (current year minus 100 years).
							$lowestyear = $year - 82;
						while ( $year >= $lowestyear ) {
							echo '<option value="' . esc_attr( $year ) . '">' . esc_html( $year ) . '</option>';
							$year--;
						}
						?>
					</select>
				</span>
			</p>
			<?php endif; ?>
			<?php if ( 'yes' === $this->show_phone ) : ?>
			<div class="clear"></div>
			<p class="form-row form-row-first validate-required validate-phone">
				<label for="<?php echo esc_attr( $this->id ); ?>_phone"><?php esc_html_e( 'Phone number', 'afterpay-payment-gateway-for-woocommerce' ); ?><span class="required">*</span></label>
				<input type="input" class="input-text" name="<?php echo esc_attr( $this->id ); ?>_phone" />
			</p>
			<?php endif; ?>
			<?php if ( 'yes' === $this->show_termsandconditions ) : ?>
			<div class="clear"></div>
			<p class="form-row validate-required">
				<input type="checkbox" class="input-checkbox" name="<?php echo esc_attr( $this->id ); ?>_terms" /><span class="required">*</span>
				<?php echo esc_html__( 'I accept the', 'afterpay-payment-gateway-for-woocommerce' ) . ' <a href="' . esc_url( $this->afterpay_invoice_terms ) . '" target="blank">' . esc_html__( 'payment terms', 'afterpay-payment-gateway-for-woocommerce' ) . '</a>' . esc_html__( ' from AfterPay.', 'afterpay-payment-gateway-for-woocommerce' ); ?>
			</p>
			<?php endif; ?>
		</fieldset>
		<?php
	}

	/**
	 * Validate form fields.
	 *
	 * @access public
	 * @return boolean
	 */
	public function validate_fields() {
		global $woocommerce;

		if ( 'yes' === $this->show_phone && ! isset( $_POST[ $this->id . '_phone' ] ) ) {
			wc_add_notice( __( 'Phone number is a required field', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( 'yes' === $this->show_dob && ! isset( $_POST[ $this->id . '_date_of_birth_day' ] ) ) {
			wc_add_notice( __( 'Date of birth is a required field', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( 'yes' === $this->show_gender && ! isset( $_POST[ $this->id . '_gender' ] ) ) {
			wc_add_notice( __( 'Gender is a required field', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( 'yes' === $this->show_termsandconditions && ! isset( $_POST[ $this->id . '_terms' ] ) ) {
			wc_add_notice( __( 'Please accept the AfterPay terms.', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		return true;
	}
}
